# coding=utf-8
from modules import *
import simplejson

class APIupdate(object):

    def __init__(self, httprequest):
        self.httprequest = httprequest


    def scheduler_in_progress(self, id):
        response = self.httprequest.put("scheduler/"+id, simplejson.dumps({"status":"in progress"}))
        return response

    def scheduler_after_succes(self, id, interval):

        now = datetime.now().replace(microsecond=0)
        execution = (now + timedelta(minutes=interval))

        response = self.httprequest.put("scheduler/"+id, simplejson.dumps({"lastUpdate":now.isoformat(),"status":"active","execution":execution.isoformat()}))
        return response
    def ci(self, httprequest):
        response = self.httprequest.post("scan", simplejson.dumps({"status": "in progress"}))