import arrow
from datetime import datetime
from datetime import timedelta

def search_in_dict(search_dict, field):

    fields_found = []

    for key, value in search_dict.iteritems():

        if key == field:
            fields_found.append(value)

        elif isinstance(value, dict):
            results = search_in_dict(value, field)
            for result in results:
                fields_found.append(result)

        elif isinstance(value, list):
            for item in value:
                if isinstance(item, dict):
                    more_results = search_in_dict(item, field)
                    for another_result in more_results:
                        fields_found.append(another_result)

    return fields_found


def parse_datetime(str):
    arrowObj = arrow.get(str)
    dt = arrowObj.datetime
    return dt.replace(tzinfo=None)



def get_todo_shedulers(list_of_shedulers):
    active_schedulers = []
    for scheduler in list_of_shedulers:
        if scheduler[0].get('status') == 'active' and parse_datetime(scheduler[0].get('execution')) < datetime.now().replace(microsecond=0):
            active_schedulers.append(scheduler)
    return active_schedulers


def ipRange(start_ip, end_ip):
    start = list(map(int, start_ip.split(".")))
    end = list(map(int, end_ip.split(".")))
    temp = start
    ip_range = []

    if start_ip == end_ip:
        ip_range.append(start_ip)
        return ip_range

    ip_range.append(start_ip)
    while temp != end:
        start[3] += 1
        for i in (3, 2, 1):
            if temp[i] == 256:
                temp[i] = 0
                temp[i - 1] += 1
        ip_range.append(".".join(map(str, temp)))

    return ip_range

def ip_range_to_list(start_ip, end_ip):
    start = list(map(int, start_ip.split(".")))
    end = list(map(int, end_ip.split(".")))
    temp = start
    ip_range = start_ip

    if start_ip == end_ip:

        return start_ip+","

    while temp != end:
        start[3] += 1
        for i in (3, 2, 1):
            if temp[i] == 256:
                temp[i] = 0
                temp[i - 1] += 1
        ip_range+=","+(".".join(map(str, temp)))

    return ip_range
