# coding=utf-8
from modules import *
from scan_snmp import ScanSNMP


class Citypes(object):
    def __init__(self, httprequest,logger):
        self.logger = logger
        self.citypes=httprequest.get('citype/')
        self.logger.info('citypes = %s',simplejson.dumps(self.citypes, indent=4))

    def get_citype_keyid_name(self):
        keyIDnames={}
        for i in self.citypes:
            # {"citype_name":"keyIDname"}
            if i.get('keyID') is not None:
                keyIDnames[i["name"]] = i['keyID']['name']

        self.logger.info('citype_keyid_names =%s',simplejson.dumps(keyIDnames, indent=4))
        return keyIDnames