# coding=utf-8
from modules import *

class Mapping(object):
    def __init__(self, mapping, httprequest):
        self.mapping = mapping
        self.httprequest = httprequest
    def get_job_type(self):
        job_type = self.mapping['task']['jobType']
        return job_type

    def get_command(self):
        command = self.mapping['task']['command']
        return command

    def get_attribute_name(self):
        attribute_name = self.mapping['attribute']['name']
        return attribute_name

    def get_citype_name(self):
        ciTypeID = self.mapping['ciTypeID']
        ciType = self.httprequest.get('citype/'+ciTypeID)
        ciType_name = ciType['name']
        return ciType_name

