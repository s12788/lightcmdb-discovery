# coding=utf-8
from modules import *

class Probe(object):
    def __init__(self, probe, logger):
        self.probe = probe
        self.logger = logger

    def get_todo_schedulers(self):

        # przefiltrowanie wszystkich schedulerów z proba
        schedulers = search_in_dict(self.probe, 'scheduler')

        if schedulers == []:
            self.logger.info('no active schedulers')
        return schedulers

        # przefiltrowanie schedulerów ze statusem do wykonania

        todo_schedulers = []
        for scheduler in schedulers:
            if scheduler[0].get('status') == 'active' and parse_datetime(
                    scheduler[0].get('execution')) < datetime.now().replace(microsecond=0):
                todo_schedulers.append(scheduler)
        return todo_schedulers


    def get_mappings(self):
        mappings = search_in_dict(self.probe, 'mapping')
        return mappings[0]

    # zamiana zakresów IP na listy (strings)

    def get_ip_lists(self):
        ipranges = search_in_dict(self.probe, 'iprange')

        ip_lists = []
        for iprange in ipranges[0]:
            ip_list = ip_range_to_list(iprange['start'], iprange['stop'])
            ip_lists.append(ip_list)

        return ip_lists