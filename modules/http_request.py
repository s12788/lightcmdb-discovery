# importing the requests library
import requests


class HttpRequest(object):
    def __init__(self, url, logger):
        self.url = url
        self.logger = logger

    def get(self, params):
        # sending get request and saving the response as response object
        resp = requests.get(self.url+params, headers={"Accept":"application/json"})
        try:
            data = resp.json()
        except Exception:
            #self.logger.info(resp.status_code)
            self.logger.info('No JSON content\n')
            exit(1)

        return data


    def post(self, params, data):
        # sending get request and saving the response as response object
        resp = requests.post(self.url+params, data, headers={"content-type": "application/json"})
        try:
            data = resp.json()
        except Exception:
            data = resp

        return data


    def put(self, params, data):
        # sending get request and saving the response as response object
        resp = requests.put(self.url+params, data, headers={"content-type": "application/json"})
        try:
            data = resp.json()
        except Exception:
            data = (resp.status_code)

