# coding=utf-8
import simplejson

from modules import *
from scan_snmp import ScanSNMP
from playbook import Playbook
from scan_winrm import ScanWinRM


class Task(object):
    def __init__(self, mapping, ip_list, config, logger):
        self.mapping = mapping
        self.ip_list = ip_list
        self.config = config
        self.result = None
        self.logger = logger

        if mapping is None:
            self.result = self.run_ping_job()
            job_type = 'ICMP ping'

        elif mapping is not None:
            job_type = self.mapping.get_job_type()

        if job_type == 'SSH':
            self.result = self.run_ssh_job()



        elif job_type == 'SNMP':
            self.result =self.run_snmp_job(logger)

        elif job_type == 'playbook':
            self.result = self.run_playbook()

        elif job_type == 'WinRM':
            self.result = self.run_powershell_job()

        self.logger.info('Job type %s\n', job_type)



    def run_ping_job(self):

        scan_ping = ScanSSH()

        result = scan_ping.run(self.ip_list, 'ping -c 3 {{ inventory_hostname }} > /dev/null 2>&1 && echo pong || echo unreachable', 'local',self.config)
        self.logger.info(simplejson.dumps((result), indent=4)+'\n')

        return result

    def run_ssh_job(self):

        scan_ssh = ScanSSH()
        result = scan_ssh.run(self.ip_list, self.mapping.get_command(),'ssh',self.config)

        return result

    def run_snmp_job(self,logger):

        scan_snmp = ScanSNMP(self.config,logger)
        result = scan_snmp.run(self.ip_list, self.mapping.get_command())
        self.logger.info(simplejson.dumps((result), indent=4))

        return result


    def run_playbook(self):

        playbook = Playbook()
        self.logger.info('-----------------------------------PRZED ANSIBLE JOBEM command:%s, ip_list:%s ',self.mapping.get_command(),self.ip_list)
        result = playbook.running(self.ip_list, self.mapping.get_command(),self.config)

        return result

    def run_powershell_job(self):
        scan_winrm = ScanWinRM()
        result = scan_winrm.run(self.ip_list, self.mapping.get_command(),self.config)


        return result