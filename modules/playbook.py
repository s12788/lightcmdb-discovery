import os
import sys
from collections import namedtuple

from ansible.parsing.dataloader import DataLoader
from ansible.vars.manager import VariableManager
from ansible.inventory.manager import InventoryManager
from ansible.executor.playbook_executor import PlaybookExecutor
from ansible.plugins.callback import CallbackBase
import simplejson

class ResultCallbackPlaybook(CallbackBase):
    """A sample callback plugin used for performing an action as results come in

    If you want to collect all results into a single object for processing at
    the end of the execution, look into utilizing the ``json`` callback plugin
    or writing your own custom callback plugin
    """

    def __init__(self, *args, **kwargs):
        super(ResultCallbackPlaybook, self).__init__(*args, **kwargs)
        self.hosts = {}


    def v2_runner_on_ok(self, result, **kwargs):
        """Print a json representation of the result

        This method could store the result in an instance attribute for retrieval later
        """
        host = result._host

        attributes = {}
        if host.name not in self.hosts:
            self.hosts.update({host.name:attributes})
        try:

            self.hosts[host.name].update({result._result['msg'][0]: result._result['msg'][1]})

        except Exception:
            pass



    def v2_runner_on_unreachable(self, result):

        host = result._host
        self.hosts.update({host.name: {"unreachable":result._result['msg'].replace("\r\n", "")}})



    def v2_runner_on_failed(self, result, ignore_errors=False):

        host = result._host
        try:
            self.hosts.update({host.name: {"unreachable":result._result['stderr']}})
        except Exception:
            self.hosts.update({host.name: {"unreachable":result._result['msg']}})


class Playbook(object):



    def running(self,iprange, playbook_path,config):

        results_callback = ResultCallbackPlaybook()

        variable_manager = VariableManager()
        loader = DataLoader()

        inventory = InventoryManager(loader=loader, sources=[iprange])
        #playbook_path = '/home/mic/ansible/test.yml'

        if not os.path.exists(playbook_path):
            print '[INFO] The playbook does not exist'
            sys.exit()

        Options = namedtuple('Options', ['listtags', 'listtasks', 'listhosts', 'syntax','module_path', 'forks', 'remote_user', 'private_key_file', 'ssh_common_args', 'ssh_extra_args', 'sftp_extra_args', 'scp_extra_args', 'become', 'become_method', 'become_user', 'verbosity', 'check', 'diff'])

        options = Options(listtags=False, listtasks=False, listhosts=False, syntax=False, module_path=None, forks=100, remote_user='vagrant', private_key_file=None, ssh_common_args=None, ssh_extra_args=None, sftp_extra_args=None, scp_extra_args=None, become=False, become_method=None, become_user='root', verbosity=None, check=False, diff=False,)

        variable_manager = VariableManager(loader=loader, inventory=inventory)
        passwords = {}

        # Instantiate our ResultCallback for handling results as they come in

        pbex = PlaybookExecutor(playbooks=[playbook_path], inventory=inventory, variable_manager=variable_manager, loader=loader, options=options, passwords=passwords)

        pbex._tqm._stdout_callback = results_callback

        try:
            result = pbex.run()
            return results_callback.hosts

        except Exception as e:
            print e
