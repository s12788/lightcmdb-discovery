# coding=utf-8
from modules import *
from scan_snmp import ScanSNMP


class Job(object):
    def __init__(self, job):
        self.job = job

    def get_mappings(self):
        mappings = self.job['mapping']
        return mappings