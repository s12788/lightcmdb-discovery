# coding=utf-8

from collections import namedtuple
from ansible.parsing.dataloader import DataLoader
from ansible.vars.manager import VariableManager
from ansible.inventory.manager import InventoryManager
from ansible.playbook.play import Play
from ansible.executor.task_queue_manager import TaskQueueManager
from ansible_result_callback import ResultCallback

class ScanSSH(object):


    def run(self, iprange, command, connection, config):
        self.config = config
        Options = namedtuple('Options', ['connection', 'module_path', 'forks', 'become', 'become_method', 'become_user', 'check', 'diff'])
    # initialize needed objects
        loader = DataLoader()
        options = Options(connection=connection, module_path='/path/to/mymodules', forks=100, become=None, become_method=None, become_user=None, check=False,
                    diff=False)
        passwords = dict(vault_pass='secret')

    # Instantiate our ResultCallback for handling results as they come in
        results_callback = ResultCallback()


    # create inventory and pass to var manager
        inventory = InventoryManager(loader=loader, sources=[iprange])
        variable_manager = VariableManager(loader=loader, inventory=inventory)
        variable_manager.extra_vars = {'host_key_checking': 'False', 'ansible_ssh_common_args': '-o StrictHostKeyChecking=no'}

    # create play with tasks
        play_source =  dict(
                name = "Ansible Play",
                user = config['ssh'].get('user'),
                hosts = 'all',
                gather_facts = 'no',
                tasks = [
                    dict(action=dict(module='shell', args=command))
                ]
            )
        play = Play().load(play_source, variable_manager=variable_manager, loader=loader)

    # actually run it
        tqm = None
        try:
            tqm = TaskQueueManager(
                    inventory=inventory,
                    variable_manager=variable_manager,
                    loader=loader,
                    options=options,
                    passwords=passwords,
                    stdout_callback=results_callback,  # Use our custom callback instead of the ``default`` callback plugin
                )
            tqm.run(play)

        finally:
            if tqm is not None:
                tqm.cleanup()


        return results_callback.hosts


