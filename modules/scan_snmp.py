import easysnmp
from easysnmp import snmp_get
import multiprocessing
from multiprocessing import Queue


class ScanSNMP(object):

    def __init__(self, config,logger):

        self.config = config
        self.logger = logger

    def worker(self, i, oid, q):


        try:
            hostname = snmp_get(oid, hostname=i, community='public', version=1, timeout=1, retries=0)
            q.put({i:hostname.value})
            self.logger.info(' **** I P **** %s', i)

        except easysnmp.exceptions.EasySNMPError as err:
            #q.put({i:err.message})
            self.logger.info(' **** E R R O R  SNMP SCAN **** ' + err.message)
            self.logger.info(' **** I P **** %s', i)
            pass


    def run(self, iprange, oid):

        iprange = iprange.rstrip(',')
        q = Queue()
        jobs = [ multiprocessing.Process(target=self.worker, args=(i, oid, q)) for i in iprange.split(",")]

        self.logger.info('SCAN SNAM IPRANGE = ' + iprange)

        for p in jobs:
            p.start()
        for p in jobs:
            p.join()

        q.put(None)
        hosts = []
        while 1:
            result = q.get()
            if result is None:
                return hosts
            hosts.append(result)


