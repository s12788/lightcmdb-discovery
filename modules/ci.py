# coding=utf-8
from modules import *
from scan_snmp import ScanSNMP


class Ci(object):
    def __init__(self, httprequest, logger):
        self.ci = {"CI":[]}
        self.active_ip = ''
        self.logger = logger
        self.httprequest = httprequest

    def add_ipaddress(self, ip_list):

        for i in ip_list:
            for k,v in i.items():
                if v == 'pong':
                    self.ci['CI'].append([{"citype": "ipaddress","attributes": {"name": k}}])
                    self.active_ip += k+','

    def add_attributess(self, attributes, mapping):
        citype_name = mapping.get_citype_name()
        attribute_name = mapping.get_attribute_name()

        job_type=mapping.get_job_type()

        if job_type != "playbook":


            for ci in self.ci["CI"]:
                # CI po pingu w postaci: [[{'attributes': {'name': u'192.168.2.173'}, 'citype': 'ipaddress'}], [{'attributes': {'name': u'192.168.2.172'}, 'citype': 'ipaddress'}], [{'attributes': {'name': u'192.168.2.171'}, 'citype': 'ipaddress'}], [{'attributes': {'name': u'192.168.2.170'}, 'citype': 'ipaddress'}]]

                if ci[0]["citype"] == "ipaddress":
                    for attribute in attributes:
                        for ip,value in attribute.items():
                            if ip == ci[0]["attributes"]["name"]:
                                aexist=False
                                self.logger.info('\n\n\n**** CI **** = %s\n', ci)
                                for citype in ci:
                                    self.logger.info('**** CItype **** = %s\n',citype)
                                    self.logger.info('citype[attributes] = %s\n', citype['attributes'])
                                    self.logger.info('attribute_name = %s', attribute_name)

                                    # update atrybutu w citype
                                    if attribute_name in citype['attributes']:
                                        citype['attributes'][attribute_name] = value
                                        aexist=True
                                        break
                                    # dodanie {attribute_name : value } do citypu
                                    if citype_name in citype['citype']:
                                        citype['attributes'].update( {attribute_name : value} )
                                        aexist=True
                                        break
                                # dodanie citypu
                                if aexist == False:
                                    ci.append({"citype": citype_name, "attributes": {attribute_name: value}})
                                    break

        else:
            for ci in self.ci["CI"]:
                    if ci[0]["citype"] == "ipaddress":
                        self.logger.info('-----------ATRTEIBUTES----------------%s', attributes)
                        for ip, values in attributes.iteritems():
                            self.logger.info('IP ----------------------------------'+ip)
                            if ip == ci[0]["attributes"]["name"]:
                                aexist = False
                                for citype in ci:

                                    # update atrybutów w citype
                                    for name,attribute in values.iteritems():
                                        if name in citype['attributes']:
                                            citype['attributes'][attribute_name] = attribute;
                                            aexist=True
                                            break
                                        # dodanie atrybutów do citypu
                                        if citype_name in citype['citype']:
                                            citype['attributes'].update({attribute_name: attribute})
                                            aexist = True
                                            break
                                # dodanie citypu
                                if aexist == False:
                                    ci.append({"citype": citype_name, "attributes": values})
                                    break