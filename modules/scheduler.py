# coding=utf-8
from modules import *

class Scheduler(object):
    def __init__(self, scheduler):
        self.scheduler = scheduler

    def get_id(self):
        scheduler_id = self.scheduler[0]['id']
        return scheduler_id

    def get_interval(self):
        interval = self.scheduler[0]['interval']
        return interval

    def get_jobs(self):
        jobs = self.scheduler[0]['job']
        return jobs