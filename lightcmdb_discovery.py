# coding=utf-8
import logging

from modules import *

import yaml
import multiprocessing
import time
import simplejson

# wczytanie pliku konfiguracyjnego
config = yaml.safe_load(open("conf/config.yml"))

logger = logging.getLogger()
url = config['API'].get('url')
probeID = config['API'].get('probeID')
httprequest = HttpRequest(url, logger)
api_update = APIupdate(httprequest)

processes = []

def run():



    # pobranie obiektu probe z API

    logger.info('\n')
    logger.info('Request for probe object...')
    probe = Probe(httprequest.get('probe/' + probeID),logger)
    logger.info('%s\n\n',simplejson.dumps((probe.probe), indent=4))

    # pobranie list IP

    logger.info('IP addresses:')
    ip_lists = probe.get_ip_lists()

    logger.info(str(ip_lists)+'\n')


    # przefiltrowanie schedulerów ze statusem do wykonania

    todo_schedulers = probe.get_todo_schedulers()

    logger.info('Active schedulers: %s\n',simplejson.dumps(todo_schedulers, indent=4))

    ci = Ci(httprequest,logger)


    for index_i, ip_list in enumerate(ip_lists):

        # wykonanie joba
        logger.info("Run ICMP ping task for IP range %s", index_i + 1)
        discovery = Task(None, ip_list, config, logger)

        ci.add_ipaddress(discovery.result)
    logger.info('%s\n', ci.ci)

    if ci.ci['CI'] == []:
        return

    # dla wszystkich schedulerów wykonaj joby

    for j in todo_schedulers:

        # tworzenie obiektów scheduler
        scheduler = Scheduler(j)


        jobs = scheduler.get_jobs()
        logger.info('Jobs %s:\n', jobs)

        for index_i,j in enumerate(scheduler.get_jobs()):

            job = Job(j)


            logger.info('Job nr %s: %s',index_i+1,simplejson.dumps(j,indent=4))

            # lista mapping (zawiera attribute, task, CiType)  przypisanych do joba z schedulera

            mappings = job.get_mappings()
            logger.info('MAPPINGS: %s\n', simplejson.dumps(mappings,indent=4))
            if mappings == []:
                logger.info('No mappings in job!\n\n')
            else:

            # zmiana statusu schedulera na active
                logger.info('API update scheduler status to in progress\n')
                api_update.scheduler_in_progress(scheduler.get_id())

            # wykonanie jobów dla list IP

                for index_j, j in enumerate(mappings):
                    logger.info('MAPPING nr %s from job nr %s: %s\n', index_j + 1,index_i+1, simplejson.dumps(j, indent=4))
                    try:
                        mapping = Mapping(j, httprequest)

                        logger.info('START Task')
                        logger.info('%s\n',ci.active_ip)
                        attributes = Task(mapping, ci.active_ip, config, logger)

                        logger.info('RESULT Task::')
                        logger.info('%s\n',simplejson.dumps(attributes.result,indent=4))
                        ci.add_attributess(attributes.result, mapping)
                        logger.info('CI after task: %s',simplejson.dumps(ci.ci['CI'],indent=4))

                    except Exception as err:
                        logger.error(err)
        citypeKeyIDnames = Citypes(httprequest,logger).get_citype_keyid_name()
        logger.info('citype_keyid_names =%s', simplejson.dumps(citypeKeyIDnames, indent=4))
        logger.info('CI before check keyIDs: %s', simplejson.dumps(ci.ci['CI'], indent=4))
        if ci.ci['CI'] != "":
            for new_ci in ci.ci['CI']:
                for citype in new_ci:
                    for citypename,keyIDname in citypeKeyIDnames.iteritems():
                        if citypename == citype["citype"]:
                            if keyIDname not in citype["attributes"]:
                                del citype["citype"]
                                del citype["attributes"]

        logger.info('CI after check keyIDs: %s', simplejson.dumps(ci.ci['CI'], indent=4))

        if ci.ci['CI'] != "":
            httprequest.post("scan", simplejson.dumps(ci.ci))
            logger.info('SEND CI to API...')

        # aktualizacja lastUpdate, statusu, execution

        api_update.scheduler_after_succes(scheduler.get_id(), scheduler.get_interval())
        logger.info('API update scheduler status\n')


def make_new_process():
    if __name__ == '__main__':
        p = multiprocessing.Process(target=run)
        processes.append(p)
        p.start()


try:
    logging.basicConfig(level=logging.DEBUG, format='%(asctime)s %(levelname)-8s %(message)s',
                        datefmt='%Y-%m-%d %H:%M:%S', filename='lightcmdb_discovery.log', filemode='a')
    logger.info('=============   starting Lightcmdb discovery module   ================\n')
    while True:
        make_new_process()
        time.sleep(config['interval'])
        logger.info('Ilosc PROCESOW: ' + (str(len(processes))))  # do wywalenia

        for p in processes:
            if not p.is_alive():
                logger.info("Is p alive %s", p.is_alive())
                processes.remove(p)

        if len(processes) >= config['max_processes']:
            logger.info('To many processes...')

            for p in processes:
                p.terminate()

            exit(1)
except KeyboardInterrupt:
    logger.info('Manual break by user\n')
except Exception as err:
    logger.info('Error, something went wrong:')
    logger.info(err.message,'\n')
